/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.zhanghai.ohos.systemuihelper.sample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 * 测试类
 *
 * @since 2021-07-26
 */
public class ExampleOhosTest {
    private Context context;
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("me.zhanghai.ohos.systemuihelper", actualBundleName);
    }
    /**
     * 开始
     *
     */
    @Before
    public void setUp () {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().getContext();
    }

    /**
     * 结束
     *
     */
    @After
    public void tearDown () {
        context = null;
    }

    /**
     * 初始化测试
     *
     */
        @Test
        public void init () {
            try {
                Class mainAbilitySlice = Class.forName("me.zhanghai.ohos.systemuihelper.slice.MainAbilitySlice");
                Method initView = mainAbilitySlice.getMethod("initView");
                Object obj = mainAbilitySlice.getConstructor().newInstance();
                initView.invoke(obj);
            } catch (ClassNotFoundException | NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
}