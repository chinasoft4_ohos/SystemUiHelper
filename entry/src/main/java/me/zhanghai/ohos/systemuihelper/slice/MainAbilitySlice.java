/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.zhanghai.ohos.systemuihelper.slice;

import me.zhanghai.ohos.systemuihelper.ResourceTable;
import me.zhanghai.ohos.systemuihelper.SystemUiHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 主页面
 *
 * @since 2021-06-21
 */
public class MainAbilitySlice extends AbilitySlice implements Component.TouchEventListener {
    private SystemUiHelper mSystemUiHelper;
    private float heigh;
    private DirectionalLayout titleLayout;
    private DirectionalLayout contentLayout;
    private final int num = 10;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
        heigh = titleLayout.getHeight();
        mSystemUiHelper = new SystemUiHelper(getAbility(),
                visible -> {
                    AnimatorValue animatorValue = new AnimatorValue();
                    animatorValue.setDuration(num);
                    animatorValue.setCurveType(Animator.CurveType.CUBIC_BEZIER_ACCELERATION);
                    if (visible) {
                        animatorValue.setValueUpdateListener((animatorValue12, vv) -> {
                            float offset = vv * heigh;
                            titleLayout.setTranslationY(offset - heigh);
                        });
                    } else {
                        animatorValue.setValueUpdateListener((animatorValue1, vv) -> {
                            float offset = vv * heigh;
                            titleLayout.setTranslationY(-offset);
                        });
                    }
                    animatorValue.start();
                });

        // This will set up window flags.
        mSystemUiHelper.show();
    }

    private void initView() {
        titleLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_titleLayout);
        contentLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_contentLayout);
        contentLayout.setTouchEventListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            mSystemUiHelper.toggle();
        }
        return false;
    }
}
