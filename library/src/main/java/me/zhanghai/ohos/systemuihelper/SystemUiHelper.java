/*
 * Copyright (c) 2015 Zhang Hai <Dreaming.in.Code.ZH@Gmail.com>
 * All Rights Reserved.
 */

package me.zhanghai.ohos.systemuihelper;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.window.service.WindowManager;

/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 主功能
 *
 * @since 2021-06-21
 */
public final class SystemUiHelper {
    private SystemUiHelperImpl mImpl;

    /**
     * 构造方法
     *
     * @param ability
     */
    public SystemUiHelper(Ability ability) {
        this(ability, null);
    }

    /**
     * 构造方法
     *
     * @param ability
     * @param listener
     */
    public SystemUiHelper(Ability ability, OnVisibilityChangeListener listener) {
        mImpl = new SystemUiHelperImpl(ability, listener);
    }

    /**
     *
     * {@link SystemUiHelper} was instantiated with.
     * @return true if the system UI is currently showing. What this means depends on the mode this
     */
    public boolean isShowing() {
        return mImpl.isShowing();
    }

    /**
     * Show the system UI. What this means depends on the mode this {@link SystemUiHelper} was
     * instantiated with.
     *
     * <p>Any currently queued delayed hide requests will be removed.
     */
    public void show() {
        // Ensure that any currently queued hide calls are removed
        mImpl.show();
    }

    /**
     * Hide the system UI. What this means depends on the mode this {@link SystemUiHelper} was
     * instantiated with.
     *
     * <p>Any currently queued delayed hide requests will be removed.
     */
    public void hide() {
        // Ensure that any currently queued hide calls are removed
        mImpl.hide();
    }

    /**
     * Toggle whether the system UI is displayed.
     */
    public void toggle() {
        // ZH: Fix: Use wrapper methods to ensure consistent behavior.
        if (isShowing()) {
            hide();
        } else {
            show();
        }
    }

    /**
     * 隐藏/显示 监听
     *
     * @since 2021-06-21
     */
    public interface OnVisibilityChangeListener {
        /**
         * Called when the system UI visibility has changed.
         *
         * @param isVisible True if the system UI is visible.
         */
        void onVisibilityChange(boolean isVisible);
    }

    /**
     * 实现类
     *
     * @since 2021-06-21
     */
    static class SystemUiHelperImpl {
        final Ability mAbility;
        final OnVisibilityChangeListener mOnVisibilityChangeListener;

        boolean mIsShowing = true;

        SystemUiHelperImpl(Ability ability,
                           OnVisibilityChangeListener onVisibilityChangeListener) {
            mAbility = ability;
            mOnVisibilityChangeListener = onVisibilityChangeListener;
        }

        boolean isShowing() {
            return mIsShowing;
        }

        void setIsShowing(boolean isShowing) {
            mIsShowing = isShowing;

            if (mOnVisibilityChangeListener != null) {
                mOnVisibilityChangeListener.onVisibilityChange(mIsShowing);
            }
        }

        void show() {
            WindowManager.getInstance().getTopWindow().get().setStatusBarVisibility(Component.VISIBLE);
            setIsShowing(true);
        }

        void hide() {
            WindowManager.getInstance().getTopWindow().get().setStatusBarVisibility(Component.INVISIBLE);
            setIsShowing(false);
        }
    }
}
