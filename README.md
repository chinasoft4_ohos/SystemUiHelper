﻿# SystemUiHelper

#### 项目介绍
- 项目名称：SystemUiHelper
- 所属系列：openharmony的第三方组件适配移植
- 功能：ohos系统UI状态栏和标题栏显示/隐藏的动画效果
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本： Release v1.0.0

#### 效果演示
<img src="img/SystemUiHelper.gif"></img>
#### 安装教程
            1.在项目根目录下的build.gradle文件中，
             allprojects {
                repositories {
                    maven {
                              url 'https://s01.oss.sonatype.org/content/repositories/releases/'
                          }
                }
             }
            2.在entry模块的build.gradle文件中，
             dependencies {
                implementation('com.gitee.chinasoft_ohos:SystemUiHelper:1.0.0')
               }
        

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明



在MainAbilitySlice.onStart()里:
   ```java
   new SystemUiHelper(getAbility(), new SystemUiHelper.OnVisibilityChangeListener() {
               @Override
               public void onVisibilityChange(boolean visible) {
                  ... 
               }
           });
  // This will set up window flags.
  mSystemUiHelper.show();
  ```
你可以参考示例应用的MainAbilitySlice来了解细节用法.
 

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
               Copyright 2015 Zhang Hai

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```